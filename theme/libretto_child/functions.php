<?php
/**
 * Add the default style
 */
add_action( 'wp_enqueue_scripts', function() {
    wp_enqueue_style( 'libretto-css', get_template_directory_uri() . '/style.css' );
}); 

add_action('author_link', function($link, $author_id, $nicename){
    return 'https://twitter.com/' . get_the_author_meta('user_login', $author_id);
}, 10, 3);